let chartData;

const cellColor = (value) => {
  colors = [
    "#082567",
    "#003399",
    "#2a52be",
    "#92a1cf",
    "#ccccff",
    "#fffafa",
    "#f4c2c2",
    "#ff6961",
    "#ce1620",
    "#b31b1b",
    "#701c1c"
  ];

  let index = 0;
  let check = -5;
  while (check < 5) {
    if (value < check + 1) return colors[index];
    check++;
    index++;
  }

  return colors[index];
}

const displayData = () => {
  const baseTemperature = chartData.baseTemperature;
  const minYear = d3.min(chartData.monthlyVariance, d => d.year);
  const maxYear = d3.max(chartData.monthlyVariance, d => d.year);

  d3.select("#title")
    .text("Monthly Global Land Surface Temperature");

  d3.select("#subtitle")
    .text(`${minYear} - ${maxYear}`);

  d3.select("#description")
    .html("Temperatures are in Celcius and reported as anomolies relative to the January 1951 - December 1980 average<br>Estimated January 1951 - December 1980 average temperature &deg;C 8.66 &plusmn;0.07");

  const margin = {
    top: 20,
    right: 20,
    bottom: 90,
    left: 125
  };
  const width = 1200 - margin.left - margin.right;
  const height = 600 - margin.top - margin.bottom;

  const svg = d3.select("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom);

  const xScale = d3.scaleLinear()
    .domain([minYear, maxYear])
    .range([margin.left, margin.left + width]);

  const yScale = d3.scaleBand()
    .domain([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
    .range([margin.top, margin.top + height])
    .round(true);

  const xAxis = d3.axisBottom(xScale);
  const yAxis = d3.axisLeft(yScale);

  const cellWidth = xScale(minYear + 1) - xScale(minYear);
  const cellHeight = yScale.bandwidth();

  svg.selectAll("rect")
    .data(chartData.monthlyVariance)
    .enter()
    .append("rect")
      .attr("class", "cell")
      .attr("fill", d => cellColor(d.variance))
      .attr("data-month", d => d.month - 1)
      .attr("data-year", d => d.year)
      .attr("data-temp", d => baseTemperature + d.variance)
      .attr("x", d => xScale(d.year))
      .attr("y", d => yScale(d.month - 1))
      .attr("width", cellWidth)
      .attr("height", cellHeight)
      .on("mouseover", d => {
        d3.select("#year").text(d.year);
        d3.select("#month").text(d3.timeFormat("%B")((new Date()).setMonth(d.month - 1)));
        d3.select("#average").text(round(baseTemperature + d.variance, 3));
        d3.select("#difference").text(Math.abs(d.variance));
        d3.select("#relation").text(d.variance < 0 ? "below" : "above");
        d3.select("#tooltip")
          .attr("data-year", d.year)
          .style("left", (d3.event.pageX + 15) + "px")
          .style("top", (d3.event.pageY + 15) + "px")
          .style("opacity", 1);
      })
      .on("mouseout", () => {
        d3.select("#tooltip")
          .style("opacity", 0);
      });

  svg.append("g")
    .attr("class", "x axis")
    .attr("id", "x-axis")
    .attr("transform", `translate(0, ${margin.top + height - 5})`)
    .call(xAxis.tickFormat(d3.format("d")));

  svg.append("g")
    .attr("class", "y axis")
    .attr("id", "y-axis")
    .attr("transform", `translate(${margin.left - 1}, 0)`)
    .call(yAxis.tickFormat(month => d3.timeFormat("%B")((new Date()).setMonth(month))));

  const legendWidth = width * 0.35;
  const legendItems = ["< -4", "-4", "-3", "-2", "-1", "0", "1", "2", "3", "4", "5 >"];

  const legendScale = d3.scaleBand()
    .domain(legendItems)
    .range([margin.left + width - legendWidth, margin.left + width]);
  const legendAxis = d3.axisBottom(legendScale);

  const legendCellSize = legendScale.bandwidth();


  svg.append("g")
    .attr("id", "legend")
    .selectAll("rect")
      .data(legendItems)
      .enter()
      .append("rect")
        .attr("fill", (d, i) => i === 0 ? cellColor(-5) : (i === 10 ? cellColor(5) : cellColor(parseInt(d))))
        .attr("x", d => legendScale(d))
        .attr("y", margin.top + height + 30)
        .attr("width", legendCellSize)
        .attr("height", legendCellSize);


  svg.append("g")
    .attr("class", "legend axis")
    .attr("transform", `translate(0, ${margin.top + height + 30 + legendCellSize})`)
    .call(legendAxis);
}

const loadData = () => {
  const req = new XMLHttpRequest();
  req.open("GET", "https://raw.githubusercontent.com/freeCodeCamp/ProjectReferenceData/master/global-temperature.json", true);
  req.onload = () => {
    chartData = JSON.parse(req.responseText);
    displayData();
  };
  req.send();
};

const round = function(number, precision = 0) {
  var shift = function(number, precision, reverseShift) {
    if (reverseShift) {
      precision = -precision;
    }

    var numArray = ("" + number).split("e");
    return +(
      numArray[0] +
      "e" +
      (numArray[1] ? +numArray[1] + precision : precision)
    );
  };

  return shift(Math.round(shift(number, precision, false)), precision, true);
};

document.addEventListener("DOMContentLoaded", loadData);
